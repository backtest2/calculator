package com.backtest.calculator.utils;

public class CalculatedResult {

	private String result;

	public CalculatedResult(String result) {
		this.result = result;
	}
	
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
	
	
	
}
