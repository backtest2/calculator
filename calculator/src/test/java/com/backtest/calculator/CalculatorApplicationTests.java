package com.backtest.calculator;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import com.backtest.calculator.models.RequestOperation;
import com.backtest.calculator.utils.CalculatedResult;
import com.backtest.calculator.utils.ErrorResponseTest;
import com.google.gson.Gson;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class CalculatorApplicationTests {

	@LocalServerPort
	private int port;
	
	@Autowired
	private TestRestTemplate restTemplate;
	
	@Test
	public void sumTest() {
		
		final String baseUrl = "http://localhost:" + port + "/calculate";
		
		RequestOperation requestOperation = new RequestOperation(Double.valueOf(2), Double.valueOf(2), "SUM");
		
		HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json"); 
		
        HttpEntity<RequestOperation> request = new HttpEntity<>(requestOperation, headers);
     
        ResponseEntity<String> result = this.restTemplate.postForEntity(baseUrl, request, String.class);
        
        Gson g = new Gson();
        
        CalculatedResult calculatedResult = g.fromJson(result.getBody(), CalculatedResult.class);
        
        Assert.assertEquals("4.0", calculatedResult.getResult());
	}

	@Test
	public void substractionTest() {
		
		final String baseUrl = "http://localhost:" + port + "/calculate";
		
		RequestOperation requestOperation = new RequestOperation(Double.valueOf(2), Double.valueOf(2), "SUBSTRACTION");
		
		HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json"); 
		
        HttpEntity<RequestOperation> request = new HttpEntity<>(requestOperation, headers);
     
        ResponseEntity<String> result = this.restTemplate.postForEntity(baseUrl, request, String.class);
        
        Gson g = new Gson();
        
        CalculatedResult calculatedResult = g.fromJson(result.getBody(), CalculatedResult.class);
        
        Assert.assertEquals("0.0", calculatedResult.getResult());
	}
	
	@Test
	public void badRequestTest() {
		
		final String baseUrl = "http://localhost:" + port + "/calculate";
		
		RequestOperation requestOperation = new RequestOperation(Double.valueOf(2), Double.valueOf(2), "SUMM");
		
		HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json"); 
		
        HttpEntity<RequestOperation> request = new HttpEntity<>(requestOperation, headers);
     
        ResponseEntity<String> result = this.restTemplate.postForEntity(baseUrl, request, String.class);
        
        Gson gson = new Gson();
        ErrorResponseTest errorResponse = gson.fromJson(result.getBody(), ErrorResponseTest.class);
        
        System.out.println(errorResponse.getErrorCode());
        
        Assert.assertEquals("BAD_REQUEST", errorResponse.getErrorCode());
        
        
	}  
}
