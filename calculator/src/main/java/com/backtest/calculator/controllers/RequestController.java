package com.backtest.calculator.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.backtest.calculator.models.RequestOperation;
import com.backtest.calculator.services.ServiceOrchestrator;
import com.backtest.calculator.utils.ErrorResponse;

import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * Request Controller class
 * @author alvaro.dieguez
 *
 */
@RestController
@RequestMapping("/calculate")
public class RequestController extends BaseControllers{

	@Autowired
	ServiceOrchestrator serviceOrchestrator;

	@ApiResponses(value = {
			@ApiResponse (code = 200 , message = "Result was successfully calculated", response = String.class),
			@ApiResponse (code = 400, message = "Bad Request", response = ErrorResponse.class)
	})
	@PostMapping(produces = {"application/json"})
	public String calculateRestult(@RequestBody RequestOperation requestOperation ) {
		return serviceOrchestrator.processRequest(requestOperation);
	}
	
}
