package com.backtest.calculator.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import com.backtest.calculator.exceptions.APIException;
import com.backtest.calculator.utils.ErrorResponse;

@RestController
public class BaseControllers {
	
	/**
	 * Exception Handler
	 * @param apiException
	 * @return
	 */
	@ExceptionHandler(value = APIException.class)
	  public ResponseEntity<ErrorResponse> handleNoSuchElementFoundException(APIException apiException) {
	    
		  return new ResponseEntity<ErrorResponse>(new ErrorResponse(apiException.getMessage(),apiException.getStatus()), apiException.getStatus());
	  }

	
}
