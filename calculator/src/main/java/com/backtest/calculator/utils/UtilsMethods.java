package com.backtest.calculator.utils;

/**
 * Class with utils methods
 * @author alvaro.dieguez
 *
 */
public class UtilsMethods {
 
	/**
	 * Method to parse a calculated result to a JSON format
	 * @param result result
	 * @return 
	 */
	public static String parseResultToJson(String result) {
		
		StringBuilder sb =  new StringBuilder();
		
		return sb.append("{\"result\" : \"").append(result).append("\"}").toString();
		
	}
	
}
