package com.backtest.calculator.utils;

import io.corp.calculator.TracerAPI;
import io.corp.calculator.TracerImpl;

/**
 * Implementation of TracerAPI
 * @author alvaro.dieguez
 *
 */
public class ImplTracerAPI implements TracerAPI{

	private TracerImpl tracerImpl;
	
	public ImplTracerAPI() {
		this.tracerImpl = new TracerImpl();
	}
	
	@Override
	public <T> void trace(T result) {
		this.tracerImpl.trace(result);
		
	}

}
