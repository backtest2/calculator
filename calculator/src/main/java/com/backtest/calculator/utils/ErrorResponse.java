package com.backtest.calculator.utils;

import org.springframework.http.HttpStatus;

/**
 * Error response POJO
 * @author alvaro.dieguez
 *
 */
public class ErrorResponse {

	private String errorMessage;
	
	private HttpStatus errorCode;
	
	public ErrorResponse(String errorMessage, HttpStatus errorCode) {
		this.errorMessage = errorMessage;
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public HttpStatus getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(HttpStatus errorCode) {
		this.errorCode = errorCode;
	}
		
	
}
