package com.backtest.calculator.utils;

/**
 * Class with the error messages
 * @author alvaro.dieguez
 *
 */
public class ErrorLibrary {

	public static final String OPERATION_TYPE_ERROR = "Operation parameter, in body json, is not correctly informed";
	
	public static final String NUMBER_IS_NOT_INFORMED = "One of the numbers is not informed";
}
