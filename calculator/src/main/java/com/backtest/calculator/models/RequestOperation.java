package com.backtest.calculator.models;

/**
 * Request POJO 
 * @author alvaro.dieguez
 *
 */
public class RequestOperation {

	private Double num1;
	
	private Double num2;
	
	private String operation;

	public RequestOperation (Double num1, Double num2, String operation) {
		this.num1 = num1;
		this.num2 = num2;
		this.operation = operation;
	}
	
	public Double getNum1() {
		return num1;
	}

	public void setNum1(Double num1) {
		this.num1 = num1;
	}

	public Double getNum2() {
		return num2;
	}

	public void setNum2(Double num2) {
		this.num2 = num2;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	
	
	
}
