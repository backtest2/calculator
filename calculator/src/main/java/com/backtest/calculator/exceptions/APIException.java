package com.backtest.calculator.exceptions;

import org.springframework.http.HttpStatus;

/**
 * API Exception class
 * @author alvaro.dieguez
 *
 */
public class APIException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1793146981647544548L;
	
	private HttpStatus status;
	
	private String message;


   public APIException(HttpStatus status, String message) {
       this.status = status;
       this.message = message;
   }

	public HttpStatus getStatus() {
		return status;
	}

	public void setStatus(HttpStatus status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	   
	   
}
 