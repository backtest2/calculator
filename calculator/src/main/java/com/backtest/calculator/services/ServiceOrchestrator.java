package com.backtest.calculator.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.backtest.calculator.exceptions.APIException;
import com.backtest.calculator.models.RequestOperation;
import com.backtest.calculator.utils.ErrorLibrary;
import com.backtest.calculator.utils.UtilsMethods;

import io.corp.calculator.TracerImpl;

/**
 * Service Orchestrator. If the arithmetic operation is a SUM this service calls the SumService.
 * On the other side, if the arithmetic operation is a SUBTRACTION this service calls the SubstractionService
 * @author alvaro.dieguez
 *
 */
@Service
public class ServiceOrchestrator {
	
	private static final String SUM = "SUM";
	
	private static final String SUBSTRACTION = "SUBSTRACTION";
	
	@Autowired
	SubstractionService substractionService;
	
	@Autowired
	SumService sumService;
	
	public String processRequest(RequestOperation requestOperation) throws APIException {
		
		if (requestOperation.getNum1() == null || requestOperation.getNum2() == null || requestOperation.getOperation() == null) {
			throw new APIException(HttpStatus.BAD_REQUEST, ErrorLibrary.NUMBER_IS_NOT_INFORMED);
		}
		
		
		TracerImpl resultTracerApi = new TracerImpl();
		
		switch (requestOperation.getOperation()) {
		
			case SUM:
				resultTracerApi.trace(sumService.calculateSum(requestOperation.getNum1(), requestOperation.getNum2()));
				return UtilsMethods.parseResultToJson(sumService.calculateSum(requestOperation.getNum1(), requestOperation.getNum2()).toString());
			case SUBSTRACTION:
				resultTracerApi.trace(substractionService.calculateSubstraction(requestOperation.getNum1(), requestOperation.getNum2()));
				return UtilsMethods.parseResultToJson(substractionService.calculateSubstraction(requestOperation.getNum1(), requestOperation.getNum2()).toString());
			default:
				throw new APIException(HttpStatus.BAD_REQUEST, ErrorLibrary.OPERATION_TYPE_ERROR);
				
		}
		
	}
	
	
}
