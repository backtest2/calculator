package com.backtest.calculator.services;

import org.springframework.stereotype.Service;

/**
 * Substraction Service
 * @author alvaro.dieguez
 *
 */
@Service
public class SubstractionService {
	
	/**
	 * Method to calculate the substration of num2 from num1
	 * @param num1
	 * @param num2
	 * @return
	 */
	public Double calculateSubstraction(Double num1, Double num2) {
		return num1-num2;
	}
	
}
