package com.backtest.calculator.services;

import org.springframework.stereotype.Service;

/**
 * Sum Service
 * @author alvaro.dieguez
 *
 */
@Service
public class SumService {

	/**
	 * Method to calculate the sum num1 and num2
	 * @param num1
	 * @param num2
	 * @return
	 */
	public String calculateSum(Double num1, Double num2) {
		
		return Double.toString(num1 + num2);
	}
	
}
